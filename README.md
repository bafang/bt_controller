The Controller should hardware should work with HC06 module from 3bay, but there's a problem with it in that it
wouldn't know when a user is connected to it without a secondary controller monitoring the led that flashes on/off
while not connected. after one second being a logic high it would switch the controller on via transistor.

would also need a 50+v to 5v DCDC converter solution, I have a large 3A dcdc converter and USB breakout I'll use
to power it during development. then unplug the power to turn the unit off.

haven't tested the current to turn it on yet so that i could pick the proper transistor.

there would be some security with it since it requires a pin to connect. and greater compatibility because it's BT 3 compatible.

I wanted a bt 4 solution so i bought HM10 module. it has the same problem with the LED flashing, and no pin to send/receive data
on mobile, however my pc would require extra software to provide a serial port. it does have an 8051 built in so with the proper firmware
could be a single module solution